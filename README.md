# OpenML dataset: Viewing-Solar-Flares

https://www.openml.org/d/43399

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The subject of this dataset is multi-instrument observations of solar flares.  There are a number of space-based instruments that are able to observe solar flares on the Sun; some instruments observe the entire Sun all the time, and some only observe part of the Sun some of the time.  We know roughly where flares occur on the Sun but we don't know when they will occur.  In this respect solar flares resemble earthquakes on Earth. This dataset is a catalog of which solar flares have been observed by currently operational space-based solar observatories.
Content
It includes that start time and end time of each solar flare from 1 May 2010 to 9 October 2017 and which instrument(s) they were observed by.  It was collected by doing a retrospective analysis of the known pointing of seven different instruments with the location and times of 12,455 solar flares.
Acknowledgements
The dataset was compiled by Dr. Ryan Milligan based on publicly available data and are freely distributed.  The citation of relevance is https://arxiv.org/abs/1703.04412.
Inspiration
This dataset represents the first attempted evaluation of how well space-based instrumentation co-ordinate when it comes to observing solar flares. We are particularly interested in understanding how often combinations of instruments co-observe the same flare.  The ultimate purpose is to try to find strategies that optimize the scientific return on solar flare data given the limited space-based instrument resources available. More often than not, our greatest understanding of these explosive events come through simultaneous observations made by multiple instruments.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43399) of an [OpenML dataset](https://www.openml.org/d/43399). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43399/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43399/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43399/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

